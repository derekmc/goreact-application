<!DOCTYPE html>
<html lang="en">
</html>
<head>
  <title>Media Uploader Upload Complete</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand" >Media Uploader</span>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="./upload.php">Upload</a></li>
        <li><a href="./display.php">View</a></li>
        <li><a href="./delete.php">Delete</a></li>
        <li><a href="./help.php">Help</a></li>
      </ul>
      <!-- <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>-->
    </div>
  </div>
</nav>

  
<div class="main-content container text-center">    
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$uploadOk = 0;
// Check if image file is a actual image or fake image
// echo "<br> Submit: " . $_POST["submit"];
if(isset($_POST["submit"])) {
  // $extension = strtolower(end(explode(".",
  $target_dir = "uploads/";
  $fileName = basename($_FILES["fileToUpload"]["name"]);
  $tmp_file = $_FILES["fileToUpload"]["tmp_name"];
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
  $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  echo "<br>File Name: \"" . $fileName . "\"<br>";
  if($fileType == "jpg"){
    // echo "File was jpg.";
    $uploadOk = 1;
  } else if($fileType == "mp4"){
    // echo "File was mp4.";
    $uploadOk = 1;
  } else {
    echo "<br>File is not 'jpg' or 'mp4'";
    $uploadOk = 0;
  }
  if($uploadOk == 0){
    echo "<br>Upload not allowed.";
  } else {
    if (file_exists($target_file)) {
      echo "<br>Cannot upload, file already exists.";
      $uploadOk = 0;
    } else {
      // echo "<br>tmp_file $tmp_file";
      // echo "<br>target_file $target_file";
      if (move_uploaded_file($tmp_file, $target_file)){
        echo "<br>'" . $fileName . "' has been uploaded.";
      } else {
        echo "<br>'" . $fileName . "' failed to upload.";
        echo "<br>Error: " . error_get_last();
      }
    }
  }
  /*
  */
  echo "<br><br>Script finished.";
} else {
  header("Location: ./index.php", TRUE, 301);
}
?>
<br>
<br>
<p style="text-align: center;">
<a href="./display.php"><button class="btn btn-primary">Done</button></a>
</p>

</div>
<footer class="container-fluid text-center">
  <p>Media Uploader by Derek McDaniel 2020</p>
</footer>

</body>
</html>
