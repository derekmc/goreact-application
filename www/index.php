<!DOCTYPE html>
<html lang="en">
</html>
<head>
  <title>Media Uploader Choose File</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand" >Media Uploader</span>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="./index.php">Upload</a></li>
        <li><a href="./display.php">View</a></li>
        <li><a href="./delete.php">Delete</a></li>
        <li><a href="./help.php">Help</a></li>
      </ul>
    </div>
  </div>
</nav>

  
<div class="main-content container text-center">    
  <form action="./upload.php" method="post" enctype="multipart/form-data">
    <h2> Media Upload </h2>
    <br>
    <p style="text-align: center;">
      Must be a 'jpg' or 'mp4' file.
    </p>
    <p style="text-align: center;">
      <input style="width: 200px; margin-left: auto; margin-right: auto;" type="file" name="fileToUpload" id="fileToUpload">
    </p>
    <input type="submit" value="Upload" class="btn btn-primary" name="submit">
  </form>
  <br>
</div>

<footer class="container-fluid text-center">
  <p>Media Uploader by Derek McDaniel 2020</p>
</footer>

</body>
</html>

