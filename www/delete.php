<!DOCTYPE html>
<html lang="en">
</html>
<head>
  <title>Media Uploader Delete</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand" >Media Uploader</span>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="./upload.php">Upload</a></li>
        <li><a href="./display.php">View</a></li>
        <li class="active"><a href="./delete.php">Delete</a></li>
        <li><a href="./help.php">Help</a></li>
      </ul>
      <!-- <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul> -->
    </div>
  </div>
</nav>

  
<div class="main-content container text-center">
  <h2> Delete Media </h2>
  <h3 style="color: red;"> Not Implemented </h3>
  <p> This functionality is not yet implemented. </p>
  <p> Check back later.</p>
</div>


<footer class="container-fluid text-center">
  <p>Media Uploader by Derek McDaniel 2020</p>
</footer>

<style>
</style>
</body>
</html>
