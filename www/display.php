<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
  </head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand" >Media Uploader</span>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="./index.php">Upload</a></li>
        <li class="active"><a href="#">View</a></li>
        <li><a href="./delete.php">Delete</a></li>
        <li><a href="./help.php">Help</a></li>
      </ul>
    </div>
  </div>
</nav>

<?php
  // Load file lists.
  $jpgs = glob("uploads/*.jpg");
  $mp4s = glob("uploads/*.mp4");
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php $i = 0; ?>
      <?php foreach($jpgs as $jpg){ ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo ++$i; ?>"
        <?php if($i == 1) echo "class='active'"?> ></li>
      <?php } ?>
      <?php foreach($mp4s as $mp4){ ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo ++$i; ?>"
        <?php if($i == 1) echo "class='active'"?> ></li>
      <?php } ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php $i = 0; ?>
      <?php foreach($jpgs as $jpg){ ?>
        <div class="item <?php if($i == 1) echo "active"; ?>" style="max-height: 280px;">
          <a href="#item<?php echo ++$i; ?>">
            <img src="<?php echo $jpg; ?>" alt="Image" height="280" style="height: 280px; object-fit:cover;">
            <div class="carousel-caption">
              <p><?php echo basename($jpg); ?></p>
            </div>
          </a>
        </div>
      <?php } ?>
      <?php foreach($mp4s as $mp4){ ?>
        <div class="item <?php if($i == 1) echo "active"; ?>" style="max-height: 280px;">
          <a href="#item<?php echo ++$i; ?>">
            <video width="100%" height="280px;">
              <source type="video/mp4" src="<?php echo $mp4; ?>">
            </video>
            <div class="carousel-caption">
              <p><?php echo basename($mp4); ?></p>
            </div>
          </a>
        </div>
      <?php } ?>
      <?php if($i == 0){ ?>
        <div class="item active">
          <img src="https://placehold.it/1200x400?text=IMAGE" alt="Image">
          <div class="carousel-caption">
            <h3>---</h3>
            <p>No Items</p>
          </div>      
        </div>
      <?php } ?>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
  
<div class="main-content container text-center">    
  <h2>Images</h2><br>
  <?php $i = 0; ?>
  <div class="row">
    <?php foreach($jpgs as $jpg){ ?>
      <div class="col-sm-4" id="item<?php echo ++$i; ?>">
        <img src="<?php echo $jpg; ?>" class="img-responsive" style="width:100%" alt="Image">
        <p><?php echo basename($jpg); ?></p>
      </div>
    <?php } ?>
  </div>
  <h2>Video</h2><br>
  <div class="row">
    <?php foreach($mp4s as $mp4){ ?>
      <div class="col-sm-4" id="item<?php echo ++$i; ?>">
        <video width="320" height="240" controls>
          <source type="video/mp4" src="<?php echo $mp4; ?>">
        </video>
        <p><?php echo basename($mp4); ?></p>
      </div>
    <?php } ?>
  </div>

</div><br>

<footer class="container-fluid text-center">
  <p>Media Uploader by Derek McDaniel 2020</p>
</footer>

</body>
</html>

