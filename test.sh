
# you should clear all uploaded files from the server manually
# or at least the files used here, before running these tests.

# adjust the base url to whatever subfolder you are deploying to,
# ie, the folder in /var/www/html.
BASE_URL=http://localhost/upload

# upload the files to the server
echo
echo "Image Upload Response: " `curl -s -F "submit=Upload" -F "fileToUpload=@testimg.jpg" $BASE_URL/upload.php`
#curl -v -k -F "submit=Upload" -F "fileToUpload=@testimg.jpg" $BASE_URL/upload.php
echo

echo
echo "Video Upload Response: " `curl -s -F "submit=Upload" -F 'fileToUpload=@testvid.mp4' $BASE_URL/upload.php`
#curl -v -F "submit=Upload" -F 'fileToUpload=@testvid.mp4' $BASE_URL/upload.php
echo

#test the file diffs
if diff -q <(curl -s $BASE_URL/uploads/testimg.jpg) testimg.jpg &>/dev/null; then
 >&2 echo "TEST PASSED: Server image file matched."
else
 >&2 echo "TEST FAILED: Server image file does not match."
fi

if diff -q <(curl -s $BASE_URL/uploads/testvid.mp4) testvid.mp4 &>/dev/null; then
 >&2 echo "TEST PASSED: Server video file matched."
else
 >&2 echo "TEST FAILED: Server video file does not match."
fi

