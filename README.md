# GoReact Media Uploader Applicant Project

This project is built with vanilla php, javascript, and html.

To run the project, simply copy the contents of the 'www' folder
to the web directory of some
php enabled web server such as Apache or nginx.  Create a folder called "uploads"
within that folder, and set permissions so that the server can read
and write files to that folder.

You may also need to set 'file_uploads = On' in your 'php.ini' file.

The extra feature I chose to implement is a responsive layout.
